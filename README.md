# Symfony 1 exercises

## Exercises to perform

### 01: Creates CRUD people

Branch: 01-create-crud-people

* Create a *people* CRUD:
   * Create `People` entity
      * People must have a non-unique name, a non-unique age, and a unique email field.
   * Create the controllers scaffold.
   * Creates the `people` table.

### 02: Creates a good frontend asset

Lets fetch the way to build from the Laravel 5.6. After this, we will apply and test the frontend in the alread created CRUD views for Peoples entity.

